import os

class Daemon:
  @staticmethod
  def enable_all_daemons():
    Daemon.__enable_network_daemon();
    Daemon.__enable_bluetooth_daemon();
  @staticmethod
  def __enable_network_daemon():
    os.system("sudo systemctl enable NetworkManager")
    
  @staticmethod
  def __enable_bluetooth_daemon():
    os.system("sudo systemctl enable bluetooth.service")
    os.system("sudo systemctl start bluetooth.service")
    
  @staticmethod
  def __enable_virt_manager_daemon():
    os.system("sudo usermod -aG kvm $USER")
    os.system("sudo usermod -aG libvirt $USER")
    os.system("sudo systemctl enable libvirtd.service")
    os.system("sudo systemctl start libvirtd")
    os.system("sudo systemctl enable libvirtd")
    
  def __enable_docker():
    os.system("sudo systemctl enable --now docker")
    os.system("sudo usermod -aG docker $USER")
    