import os

from logger import Logger, LoggerStatus

class GitInstall:
  @staticmethod
  def Nordzy_cursors():
    Logger.add_record("[+] Installing Nordzy-cursors", status=LoggerStatus.SUCCESS)
    GitInstall.clone("Nordzy-cursors.git")
    os.system("cd Nordzy-cursors && makepkg -si")
    os.system("cd ~ && rm -rf Nordzy-cursors")
    
  @staticmethod
  def Nordzy_icon():
    Logger.add_record("[+] Installing Nordzy-icon", status=LoggerStatus.SUCCESS)
    GitInstall.clone("Nordzy-icon.git")
    os.system("cd Nordzy-icon && makepkg -si")
    os.system("cd ~ && rm -rf Nordzy-icon")
  
  
  @staticmethod
  def clone(link: str):
   os.system("cd ~")
   os.system(f"git clone {link}")