import os
import packages

from logger import Logger, LoggerStatus
from creators.software import AurBuilder, FirefoxCustomize
from creators.patches import PatchSystemBugs
from creators.daemons import Daemons
from creators.git_install import GitInstall


class SystemConfiguration:
    def start(*args):
        start_text = f"[+] Starting assembly. Options {args}"
        Logger.add_record(start_text, status=LoggerStatus.SUCCESS)
        SystemConfiguration.__prepare_multilib()
        SystemConfiguration.__update_multilib_repo()
        if args[0]: SystemConfiguration.__start_option_1()
        if args[1]: SystemConfiguration.__start_option_2()
        if args[2]: SystemConfiguration.__start_option_3()
        if args[3]: SystemConfiguration.__start_option_4()
        if args[4]: SystemConfiguration.__start_option_5()
        if args[5]: SystemConfiguration.__start_option_6()
        if args[6]: SystemConfiguration.__start_option_7()
        Daemons.enable_all_daemons()
        PatchSystemBugs.enable_all_patches()

    @staticmethod
    def __start_option_1():
        SystemConfiguration.__create_default_folders()
        SystemConfiguration.__copy_bspwm_dotfiles()

    @staticmethod
    def __start_option_2():
        Logger.add_record("[+] Updates Enabled", status=LoggerStatus.SUCCESS)
        os.system("sudo pacman -Sy")

    @staticmethod
    def __start_option_3():
        Logger.add_record("[+] Installed BSPWM Dependencies", status=LoggerStatus.SUCCESS)
        AurBuilder.build()
        SystemConfiguration.__install_pacman_package(packages.BASE_PACKAGES)
        SystemConfiguration.__install_aur_package(packages.AUR_PACKAGES)
        FirefoxCustomize.build()

    @staticmethod
    def __start_option_4():
        Logger.add_record("[+] Installed Dev Dependencies", status=LoggerStatus.SUCCESS)
        SystemConfiguration.__install_pacman_package(packages.DEV_PACKAGES)
        SystemConfiguration.__install_pacman_package(packages.GNOME_OFFICIAL_TOOLS)
        
    @staticmethod
    def __start_option_5():
      Logger.add_record("[+] Installed Virt Manager", status=LoggerStatus.SUCCESS)
      SystemConfiguration.__install_pacman_package(packages.VIRT_MANAGER)
      Daemons.__enable_virt_manager_daemon()
      
    @staticmethod
    def __start_option_6():
        Logger.add_record("[+] Installed Nord appearance", status=LoggerStatus.SUCCESS)
        GitInstall.Nordzy_cursors()
        
    def __start_option_7():
        Logger.add_record("[+] Installed Programming language", status=LoggerStatus.SUCCESS)
        SystemConfiguration.__install_pacman_package(packages.PROGRAMMING)
        Daemons.__enable_docker()

    @staticmethod
    def __install_pacman_package(package_names: list):
        for package in package_names:
            os.system(f"sudo pacman -S --needed --noconfirm {package}")
            Logger.add_record(f"Installed: {package}", status=LoggerStatus.SUCCESS)

    @staticmethod
    def __install_aur_package(package_names: list):
        for package in package_names:
            os.system(f"yay -S --noconfirm {package}")
            Logger.add_record(f"Installed: {package}", status=LoggerStatus.SUCCESS)

    @staticmethod
    def __create_default_folders():
        Logger.add_record("[+] Create default directories", status=LoggerStatus.SUCCESS)
        default_folders = "~/Videos ~/Documents ~/Downloads " + \
                          "~/Music ~/Desktop"
        os.system("mkdir -p ~/.config")
        os.system(f"mkdir -p {default_folders}")
        os.system("cp -r Images/ ~/")

    @staticmethod
    def __copy_bspwm_dotfiles():
        Logger.add_record("[+] Copy Dotfiles & GTK", status=LoggerStatus.SUCCESS)
        os.system("cp -r config/* ~/.config/")
        os.system("cp resorces/Xresources ~/.Xresources")
        os.system("cp resorces/gtkrc-2.0 ~/.gtkrc-2.0")
        os.system("cp -r local ~/.local")
        os.system("cp -r themes ~/.themes")
        os.system("cp resorces/xinitrc ~/.xinitrc")
        os.system("cp -r bin/ ~/")
        
    @staticmethod
    def __prepare_multilib():
        Logger.add_record("[+] Prepare Multilib", status=LoggerStatus.SUCCESS)
        os.system(r"sudo sed -i 's/^#\[multilib\]/[multilib]/' /etc/pacman.conf")
        os.system(r"sudo sed -i '/^\[multilib\]$/,/^\[/ s/^#\(Include = \/etc\/pacman\.d\/mirrorlist\)/\1/' /etc/pacman.conf")

    @staticmethod
    def __update_multilib_repo():
        Logger.add_record("[+] Update Multilib Repository", status=LoggerStatus.SUCCESS)
        os.system("sudo pacman -Sl multilib")
        os.system("sudo pacman -Sy")
