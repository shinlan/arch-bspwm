

BASE_PACKAGES = [
    "tumbler", "ffmpegthumbnailer", "lsd", "alacritty", "bat", "brightnessctl", "calc",
    "automake", "blueman", "bluez", "bluez-utils", "dunst", "fakeroot", "feh", "firefox",
    "fish", "dpkg", "gcc", "gedit", "git", "gnu-netcat", "htop", "btop", "nano", "lxappearance",
    "mat2", "mpd", "mpv", "thunar", "ncmpcpp", "neofetch", "network-manager-applet", "nitrogen",
    "pamixer", "papirus-icon-theme", "pavucontrol", "polybar", "autoconf", "mpc", "pulseaudio",
    "pulseaudio-alsa", "python-pyalsa", "ranger", "redshift", "reflector", "rofi", "rofi-calc", "calcurse",
    "rofi-emoji", "scrot", "sudo", "slop", "tree", "unrar", "zip", "unzip", "uthash", "xarchiver",
    "xfce4-power-manager", "xfce4-settings", "xorg-xbacklight", "zathura", "zathura-djvu", "zathura-pdf-mupdf",
    "cmake", "clang", "gzip", "imagemagick", "make", "openssh", "pulseaudio-bluetooth", "shellcheck",
    "vlc", "usbutils", "picom", "networkmanager-openvpn", "alsa-plugins", "alsa-tools", "alsa-utils", "ffmpeg",
    "p7zip", "gparted", "sshfs", "openvpn", "xclip", "gpick", "wget", "ueberzug", "netctl", "light", "libreoffice",
    "breeze", "amd-ucode", "ttf-jetbrains-mono", "ttf-jetbrains-mono-nerd", "ttf-fira-code",
    "ttf-iosevka-nerd", "mesa", "lib32-mesa", "gnome-calendar"
]

DEV_PACKAGES = [
    "cheese", "screenkey", "timeshift", "pinta", "kdenlive", "wireshark-qt",
    "filezilla", "ghex", "chromium", "keepassxc", "audacity", "gufw", "python-pywal",
    "bleachbit", "veracrypt", "homebank", "gtkhash", "gnome-firmware", "touche", "dconf-editor",
    "neovim", "obs-studio", "telegram-desktop", "tmux", "youtube-dl", "code", "cowsay",
    "deluge-gtk", "flameshot", "sqlitebrowser", "python-pip", "bpython", "ipython", "cloc",
]

AUR_PACKAGES = [
    "cava", "i3lock-color", "ptpython", "obsidian", "google-chrome"
]

GNOME_OFFICIAL_TOOLS = [
    "evince", "gnome-calculator", "gnome-disk-utility", "gucharmap",
    "gthumb", "gnome-clocks"
]

VIRT_MANAGER = [ "virt-manager", "qemu", "bridge-utils", "dnsmasq"]

PROGRAMMING = ["nodejs-lts-iron", "npm", "docker", "docker-compose"]