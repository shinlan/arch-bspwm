# Java

```
yay (extra/jre8-openjdk, extra/jre17-openjdk, extra/jre11-openjdk, extra/jre-openjdk)
micro ~/.config/fish/config.fish
(if status is-interactive
    set -x JAVA_HOME /usr/lib/jvm/default-runtime/
    set -x PATH $JAVA_HOME/bin $PATH
end)
```
