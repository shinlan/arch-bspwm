Перед запуском `install.sh` необходимо:

Если надо WIFI (должен быть установлен networkmanager):

```
nmcli device wifi list
nmcli device wifi connect <network-name> password <password>
ping google.com
```

---

```
cd
mkdir Downloads
git clone https://gitlab.com/shinlan/arch-bspwm.git ./Downloads/arch
```
