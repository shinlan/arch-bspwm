# Установка fish

```
sudo pacman -S fish
fish
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install jorgebucaran/nvm.fish
fisher install IlanCosman/tide@v5
chsh -s /usr/bin/fish
set -U fish_greeting
```

# Установка YAY

```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

# Установка VirtMachine

```
sudo pacman -S virt-manager qemu bridge-utils dnsmasq
sudo usermod -aG kvm $USER
sudo usermod -aG libvirt $USER
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
```

# Подключение multilib

```
sudo nano /etc/pacman.conf

необходимо раскоментировать вот это:
[multilib]
Include = /etc/pacman.d/mirrorlist
```

yay vscode vivaldi obsidian

cd theme ==> cd -icon ==> sudo ./install.sh
