# Classic arch

Проверка EFI:

```
cat /sys/firmware/efi/fw_platform_size
```

Если нет такого, значит у тебя Secure boot в биосе включен, вырубай

Инет и тд:

```
ping archlinux.org

# если инета нет то -
iwctl

device list # ищем свой девайс (wlan0)

station <устройство> scan

station <устройство> get-networks # ищем точку вайфая

station <устройство> connect <сеть нзвание TP_LINK_228>

ping archlinux.org # снова проверяем

timedatectl set-ntp true
```

Разбивка диска:

```
fdisk -l

cfdisk /dev/***
# там надо разбивку сделать (550M EFI, сколько-то swap, остальное в корень и типы сделать 1 - EFI, 2 - swap, 3 - не трогать)

fdisk -l # смотрим что натворил
```

Форматирование:

```
mkfs.fat -F32 /dev/<до EFI>
mkfs.btrfs -f /dev/<до корня>
mkswap /dev/<до swap>
```

Монтирование:

```
mount /dev/<до корня> /mnt

mount --mkdir /dev/<до EFI> /mnt/boot

swapon /dev/<до swap>
```

Установка:

```
pacstrap -K /mnt base base-devel linux-zen linux-zen-headers btrfs-progs realtime-privileges amd-ucode (intel-ucode) linux-firmware

genfstab -U /mnt > /mnt/etc/fstab
```

Разборки в системе:

```
arch-chroot /mnt

ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime

hwclock --systohc

sudo pacman -S refind nano vim micro git networkmanager os-prober efibootmgr lightdm lightdm-gtk-greeter

sudo nano /etc/locale.gen (раскоментить en_US.UTF-8 UTF-8 & ru_RU.UTF-8 UTF-8)

sudo echo "LANG=en_US.UTF-8" > /etc/locale.conf
locale-gen

sudo echo "KEYMAP=ru
FONT=cyr-sun16" > /etc/vconsole.conf

sudo echo "shinPC" > /etc/hostname # вместо shinPC свой название пк

sudo echo "127.0.0.1   localhost
	::1         localhost
	127.0.1.1   shinPC" > /etc/hosts # вместо shinPC свой пк в прошлой команде

passwd

useradd -m shin # вместо shin свой ник

passwd shin # вместо shin свой ник

usermod -aG wheel,audio,optical,storage shin # вместо shin свой ник

userdbctl groups-of-user shin # вместо shin свой ник

pacman -S sudo

EDITOR=nano

visudo (раскоментить строку %wheel ALL=(ALL:ALL) ALL

systemctl enable NetworkManager lightdm

systemctl mask NetworkManager-wait-online

refind-install
```

Меняем конфиг refind:

```
nano /boot/refind_linux.conf
# тут оставляем только строку "Boot with minimal
options". Тут по идее будет "ro root=UUID=*ид диска с /, как /dev/vda3 здесь*"
```

Конец:

```
exit
umount -l /mnt
reboot
```

после переходим в другую сессию ctrl + alt + f3, и ставим что хотим

```
sudo pacman -S xorg xorg-server gnome
sudo pacman -S xorg xorg-server xfce4
sudo pacman -S xorg xorg-server plasma
```
