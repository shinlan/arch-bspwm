#!/bin/bash

while true; do
    network_interface=$(ip route | grep '^default' | awk '{print $5}' | head -n1)
    export NETWORK_INTERFACE=$network_interface
    sleep 10
done
