#!/bin/bash

while getopts n:e: flag
do
    case "${flag}" in
        n) name=${OPTARG};;
        e) email=${OPTARG};;
    esac
done

sudo pacman -S --needed git xclip

ssh-keygen -t ed25519 -C "$email"
eval "$(ssh-agent -s)"
ssh-add   ~/.ssh/id_ed25519
cat ~/.ssh/id_ed25519.pub
cat ~/.ssh/id_ed25519.pub | xclip -selection clipboard


git config --global user.name "$name"
git config --global user.email "$email"
