sudo pacman -Syu
sudo pacman -S --needed xorg xorg-server xorg-xinit alacritty jq bspwm polybar rofi sxhkd picom xautolock ttf-nerd-fonts-symbols ttf-jetbrains-mono-nerd ttf-jetbrains-mono pulseaudio pavucontrol noto-fonts-cjk noto-fonts-emoji nitrogen telegram-desktop firefox shutter nemo curl tar bluez bluez-utils bluedevil blueman sddm network-manager-applet qbittorrent neofetch htop libreoffice lxappearance awesome-terminal-fonts flameshot brightnessctl 

echo "Section \"InputClass\"
    Identifier \"system-keyboard\"
    MatchIsKeyboard \"on\"
    Option \"XkbLayout\" \"us,ru\"
    Option \"XkbModel\" \"pc105\"
    Option \"XkbOptions\" \"grp:alt_shift_toggle,grp:win_space_toggle\"
EndSection" | sudo tee /etc/X11/xorg.conf.d/00-keyboard.conf

echo "Section \"InputClass\"
    Identifier \"libinput touchpad catchall\"
    MatchIsTouchpad \"on\"
    MatchDevicePath \"/dev/input/event*\"
    Option \"NaturalScrolling\" \"on\"
    Driver \"libinput\"
    Option \"Tapping\" \"on\"
    Option \"MiddleEmulation\" \"on\"
    Option \"DisableWhileTyping\" \"off\"
EndSection" | sudo tee /etc/X11/xorg.conf.d/50-touchpad.conf


cp -r ~/Downloads/arch/.config ~/
cp -r ~/Downloads/arch/.themes ~/
cp -r ~/Downloads/arch/.wallpaper ~/

echo "exec sxhkd &
exec bspwm" | sudo tee ~/.xinitrc

mkdir  ~/Documents
mkdir  ~/Music
mkdir  ~/Pictures

cd ~
git clone https://github.com/alvatip/Nordzy-cursors
cd Nordzy-cursors
./install.sh

cd ~
git clone https://github.com/alvatip/Nordzy-icon
cd Nordzy-icon/
./install.sh

sudo systemctl enable bluetooth

sudo pacman -S nodejs-lts-iron npm docker docker-compose gnome-keyring polkit-gnome

sudo systemctl enable --now docker
sudo usermod -aG docker $USER

sudo pacman -S lightdm lightdm-gtk-greeter
sudo systemctl enable lightdm